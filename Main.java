package BankAccount;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        final Account accountVTB =
                new Account("123456789",
                        "Sumka Drew", 132100);
        final Account.Card cardVTB =
                accountVTB.new Card("4800 1674 5678 9000");

        final Account accountSber =
                new Account("923842344",
                        "Golubin Gleb", 27800);
        final Account.Card cardSber =
                accountVTB.new Card("2340 7123 6145 0013");
        System.out.println("Выберите аккаунт \n" +
                "1 - Сбербанк \n" +
                "2 - ВТБ");
        int chooseAccount = sc.nextInt();

        int retry;
        String errorMessageDeposit = "Неверная сумма для пополнения!";
        String errorMessageWithdraw = "Неверная сумма для вывода!";

        do {

            // Для аккаунта Сбербанк

            if (chooseAccount == 1) {
                System.out.println("Выберите карту \n" +
                        "1 - Мир \n" +
                        "2 - MasterCard");
                int chooseCard = sc.nextInt();

                System.out.println("Выберите операцию \n" +
                        "1 - Пополнение \n" +
                        "2 - Вывод");
                int chooseOperation = sc.nextInt();
                // Пополнение карты Мир
                if (chooseCard == 1 && chooseOperation == 1) {
                    System.out.println("Введите сумму для пополнения");
                    int depositMoney = sc.nextInt();
                    if (cardSber.deposit(depositMoney) == 0) {
                        System.out.println(errorMessageDeposit);
                    }
                    System.out.println("Текущий баланс карты:");
                    System.out.println(accountSber.getAmount());
                }
                // Вывод с карты Мастеркард
                if (chooseCard == 2 && chooseOperation == 2) {
                    System.out.println("Введите суммы для вывода");
                    int withdrawMoney = sc.nextInt();
                    if (cardSber.withdraw(withdrawMoney) == 0) {
                        System.out.println(errorMessageWithdraw);
                    }
                    System.out.println("Текущий баланс карты:");
                    System.out.println(accountSber.getAmount());
                }
            }

            // Для аккаунта ВТБ

            if (chooseAccount == 2) {
                System.out.println("Выберите операцию \n" +
                        "1 - Пополнение \n" +
                        "2 - Вывод");
                int chooseOperation = sc.nextInt();
                // Пополенние
                if (chooseOperation == 1) {
                    System.out.println("Введите сумму для пополнения");
                    int depositMoney = sc.nextInt();
                    if (cardVTB.deposit(depositMoney) == 0) {
                        System.out.println(errorMessageDeposit);
                    }
                    System.out.println("Текущий баланс карты:");
                    System.out.println(accountVTB.getAmount());
                }
                // Вывод
                if (chooseOperation == 2) {
                    System.out.println("Введите суммы для вывода");
                    int withdrawMoney = sc.nextInt();
                    if (cardVTB.withdraw(withdrawMoney) == 0) {
                        System.out.println(errorMessageWithdraw);
                    }
                    System.out.println("Текущий баланс карты:");
                    System.out.println(accountVTB.getAmount());
                }
            }

            // Цикл повторения операций

            System.out.println("Хотите продолжить операции? \n" +
                    "1 - Да \n" +
                    "2 - Нет");
            retry = sc.nextInt();

        } while (retry == 1);
    }
}

