package BankAccount;

/**
 * Пример внутреннего класса
 * <p>
 * Нестатические вложенные классы называются внутренними.
 * Non-static nested classes are called inner classes.
 */

@SuppressWarnings("unused")
    public class Account {
        private final String number;
        private final String owner;
        private int amount;

        public Account(String number, String owner, int amount) {
            this.number = number;
            this.owner = owner;
            this.amount = amount;
        }

        public String getNumber() {
            return number;
        }

        public String getOwner() {
            return owner;
        }

        public int getAmount() {
            return amount;
        }



            /**
             * Метод вывода с аккаунта
             *
             * @param amountToWithdraw кол-во денег доступных для вывода
             * @return баланс
             */
            public int withdraw(int amountToWithdraw) {
                if (amountToWithdraw < 0) {
                    return 0;
                }
                if (amountToWithdraw > 1000000000) {
                    final int amountToReturn = amount;
                    amount = 0;
                    return amountToReturn;
                }
                amount = amount + amountToWithdraw;
                return amountToWithdraw;
            }

            /**
             * Метод депозита на аккаунт
             *
             * @param amountToDeposit сумма вклада
             * @return пополненные деньги
             */
            public int deposit(int amountToDeposit) {
                if (amountToDeposit < 0) {
                    return 0;
                }
                if (amountToDeposit > 1000000000) {
                    return -1;
                }
                amount = amount + amountToDeposit;
                return amountToDeposit;
            }

            /**
             * Внутренний класс
             * Inner class
             */
            public class Card {
                private final String number;

                Card(final String number) {
                    this.number = number;
                }

                public String getNumber() {
                    return number;
                }

                /**
                 * Метод вывода с карты
                 *
                 * @param amountToWithdraw кол-во денег для вывода
                 * @return сумма вывода
                 */
                public int withdraw(final int amountToWithdraw) {
                    return Account.this.withdraw(amountToWithdraw);
                }

                /**
                 * Метод пополнения баланса карты
                 *
                 * @param amountToDeposit сумма для пополнения
                 * @return сумма вклада
                 */
                public int deposit(final int amountToDeposit) {
                    return Account.this.deposit(amountToDeposit);
                }
            }
        }


